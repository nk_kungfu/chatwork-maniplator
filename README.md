# Description
Script of comment to Chatwork

# How to use
```
# anyone
git clone git@bitbucket.org:nk_kungfu/chatwork-maniplator.git
# or 
git clone https://nk_kungfu@bitbucket.org/nk_kungfu/chatwork-maniplator.git
cd chatwork-maniplator

# execute script
# 1.
./chatwork_wrapper.py -r {room_id} -m {message} -t {your api token}
# 2. to friends
./chatwork_wrapper.py -r {room_id} -m {message} -t {your api token} -f 0000000 1234567
```