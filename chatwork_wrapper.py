#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import argparse
import urllib
import urllib2
from datetime import datetime

import logging
logging.basicConfig(level=logging.INFO)


class ChatworkWrapper:
    """
    """
    api_base_uri = r"https://api.chatwork.com/v2"

    def __init__(self, token):
        """
        set token to headers.
        @param  token:    str
        """
        self.headers = {'X-ChatWorkToken': token}

    def post_comment(self, room_id, msg, to_account_ids=None):
        """
        post comment to room.
        @param  room_id:    str
        @param  msg:        str
        @param  to_account_ids: list
        """
        url = self.api_base_uri + "/rooms/{0}/messages".format(room_id)

        # exist To
        if to_account_ids:
            to_msg = "、".join(
                ["[To:{a_id}]{name}さん".format(
                    a_id=a_id, name=self.__get_user(a_id)['name'].encode('utf_8')
                ) for a_id in to_account_ids]
            )
            msg = to_msg + "\n" + msg.encode('utf_8')

        res = self.__request(url, {'body': msg}, self.headers)
        if res.code == 200:
            logging.info("posted comment({0})".format(res.code))
        else:
            logging.warning("check post_comment({0})".format(res.code))

    def add_task(self, room_id, msg, to_account_ids):
        """
        add task to room.
        @param  room_id:    str
        @param  msg:        str
        @param  to_account_ids: list
        """

        today = datetime.now().strftime('%s')

        url = self.api_base_uri + "/rooms/{0}/tasks".format(room_id)
        res = self.__request(
                    url,
                    {
                        'body': msg,
                        'to_ids': ','.join(to_account_ids),
                        'limit': today
                    },
                    self.headers
                )

    def __get_user(self, account_id):
        """
        get user data from my contacts.
        @param  account_id: str/int
        @return :           dict
        """
        url = self.api_base_uri + "/contacts"
        res = self.__request(url, headers=self.headers)
        contacts = json.loads(res.read())
        for contact in contacts:
            if int(account_id) == contact['account_id']:
                logging.info("get user data({0})".format(res.code))
                return contact
        # if no contact, get my account
        url = self.api_base_uri + "/me"
        res = self.__request(url, headers=self.headers)
        logging.info("get my data({0})".format(res.code))
        return json.loads(res.read())

    def __request(self, url, value=None, headers=None):
        """
        request with urllib and urllib2
        @param  url:        str
        @param  value:      dict
        @param  headers:    dict
        @return responce:   object
        """
        data = None
        if value:
            data = urllib.urlencode(value)

        req = urllib2.Request(url, data, headers)
        return urllib2.urlopen(req)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Comment to Chatwork.')
    parser.add_argument('-t', '--token', required=True, help='-t 1234567asdfghjk')
    parser.add_argument('-r', '--room_id', required=True, help='-r 0000000')
    parser.add_argument('-m', '--message', required=True, help='-m hello')
    parser.add_argument('-f', '--to_friends', nargs='*', default=[], help='-f 000000 111111')
    parser.add_argument(
        '--mode',
        default='post_comment',
        choices=['post_comment', 'add_task'],
        help='--mode post_comment, --mode add_task'
    )
    args = parser.parse_args()

    cw = ChatworkWrapper(args.token)
    eval('cw.{}'.format(args.mode))(args.room_id, args.message, to_account_ids=args.to_friends)
